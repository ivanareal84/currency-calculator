﻿var app = angular.module("app", []);

app.controller('ConversionController', function ($scope, $http) {

    function MoneyControl(currency, amount, selected, valid) {
        this.currency = currency;
        this.amount = amount;
        this.selected = selected;
        this.valid = valid;
        this.currencySet = ['EUR', 'RSD', 'USD'];
    }

    function AppState() {
        this.leftValue = new MoneyControl('EUR', 1, true, true);
        this.rightValue = new MoneyControl('RSD', 1, false, true);
    }

    function restoreState() {
        var myCookie = getCookie('CurrencyCalculator');
        if (myCookie == '') {
            $scope.appState = new AppState();
            createCookie('CurrencyCalculator', JSON.stringify($scope.appState), 100);
        }
        else {
            var appStateJsonStr = getCookie('CurrencyCalculator');
            $scope.appState = JSON.parse(appStateJsonStr);
        }

        loadExchageRateList();
    }


    function saveState() {
        var appStateJson = JSON.stringify($scope.appState);
        createCookie('CurrencyCalculator', appStateJson, 100);
    }

    $scope.allCurrencies = new Set(['EUR', 'RSD', 'USD']);

    $scope.leftCurrencyList = function () {
        return $scope.appState.leftValue.currencySet;
    }

    $scope.rightCurrencyList = function () {
        return $scope.appState.rightValue.currencySet;
    }

    $scope.leftClass = function () {
        var cls = ''
        if ($scope.appState.leftValue.selected) {
            cls += 'highlighted';
        }
        if (!$scope.appState.leftValue.valid) {
            cls += ' wrong-amount';
        }
        return cls.trimLeft();
    }

    $scope.rightClass = function () {
        var cls = ''
        if ($scope.appState.rightValue.selected) {
            cls += 'highlighted';
        }
        if (!$scope.appState.rightValue.valid) {
            cls += ' wrong-amount';
        }
        return cls.trimLeft();
    }

    $scope.onLeftSelected = function () {
        $scope.appState.leftValue.selected = true;
        $scope.appState.rightValue.selected = false;
        saveState();
    }

    $scope.onRightSelected = function () {
        $scope.appState.leftValue.selected = false;
        $scope.appState.rightValue.selected = true;
        saveState();
    }

    // Example EUR 100 -> RSD ?
    function conversion(fromCurrency, toCurrency, amount) {
        var from = fromCurrency.toLowerCase();
        var to = toCurrency.toLowerCase();

        var fromRecord = $scope.exchangeRatesTable[from];
        var toRecord = $scope.exchangeRatesTable[to];
        var buyRatio = fromRecord.kup;
        if (isBlank(buyRatio)) {
            buyRatio = fromRecord.sre;
        }
        buyRatio = parseFloat(buyRatio);

        var sellRatio = toRecord.pro;
        if (isBlank(sellRatio)) {
            sellRatio = toRecord.sre;
        }
        sellRatio = parseFloat(sellRatio);
        return round2(amount * buyRatio / sellRatio);
    };

    $scope.onChangeLeftCurrency = function () {
        var newRightCurrencySet = new Set($scope.allCurrencies);
        newRightCurrencySet.delete($scope.appState.leftValue.currency);

        $scope.appState.leftValue.currencySet = Array.from($scope.allCurrencies);
        $scope.appState.rightValue.currencySet = Array.from(newRightCurrencySet);
        if ($scope.appState.leftValue.currency == $scope.appState.rightValue.currency) {
            $scope.appState.rightValue.currency = $scope.appState.rightValue.currencySet[0];
        }
        $scope.onChangeLeft();
    }

    $scope.onChangeRightCurrency = function () {
        if ($scope.appState.rightValue.currency == null) {
            return;
        }
        var newLeftCurrencySet = new Set($scope.allCurrencies);
        newLeftCurrencySet.delete($scope.appState.rightValue.currency);

        $scope.appState.rightValue.currencySet = Array.from($scope.allCurrencies);
        $scope.appState.leftValue.currencySet = Array.from(newLeftCurrencySet);
        if ($scope.appState.rightValue.currency == $scope.appState.leftValue.currency) {
            $scope.appState.leftValue.currency = $scope.appState.leftValue.currencySet[0];
        }
        $scope.onChangeRight();
    }

    $scope.onChangeLeft = function () {
        var amount = $scope.appState.leftValue.amount;
        $scope.appState.leftValue.valid = isBlank(amount) || isNumber(amount);

        if (isBlank(amount) || !isNumber(amount)) {
            $scope.appState.rightValue.amount = 1;
            $scope.appState.rightValue.valid = true;
            return;
        }
        var newAmount = conversion($scope.appState.leftValue.currency,
            $scope.appState.rightValue.currency, $scope.appState.leftValue.amount);
        $scope.appState.rightValue.amount = newAmount;
        $scope.appState.rightValue.valid = true;
        saveState();
    };

    $scope.onChangeRight = function () {
        var amount = $scope.appState.rightValue.amount;
        $scope.appState.rightValue.valid = isBlank(amount) || isNumber(amount);

        if (isBlank(amount) || !isNumber(amount)) {
            $scope.appState.leftValue.amount = 1;
            $scope.appState.leftValue.valid = true;
            return;
        }
        var newAmount = conversion($scope.appState.rightValue.currency,
            $scope.appState.leftValue.currency, $scope.appState.rightValue.amount);
        $scope.appState.leftValue.amount = newAmount;
        $scope.appState.leftValue.valid = true;
        saveState();
    };


    $scope.swap = function () {
        var tmp = $scope.appState.leftValue;
        $scope.appState.leftValue = $scope.appState.rightValue;
        $scope.appState.rightValue = tmp;

        saveState();
    }

    $scope.reset = function () {
        $scope.appState.leftValue.currency = 'EUR';
        $scope.appState.rightValue.currency = 'RSD';
        $scope.appState.leftValue.amount = 1;

        $scope.onChangeLeftCurrency();
        $scope.onChangeLeft();
        $scope.onLeftSelected();

        $scope.appState.leftValue.valid = true;
        $scope.appState.rightValue.valid = true;

        saveState();

    }

    

    /*
     * Utility functions
     */
    function round2(value) {
        return value.toFixed(2);
    }

    function isBlank(value) {
        return value == undefined || value == "";
    }

    function isNumber(value) {
        return !isNaN(value);
    }

    function isAmountValid(value) {
        return !isBlank(value) && isNumber(value);
    }

    function createCookie(name, value, days) {
        var expires;
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toGMTString();
        }
        else {
            expires = "";
        }
        document.cookie = name + "=" + value + expires + "; path=/";
    }

    function getCookie(cname) {
        if (document.cookie.length > 0) {
            var cstart = document.cookie.indexOf(cname + "=");
            if (cstart != -1) {
                cstart = cstart + cname.length + 1;
                var cend = document.cookie.indexOf(";", cstart);
                if (cend == -1) {
                    cend = document.cookie.length;
                }
                return unescape(document.cookie.substring(cstart, cend));
            }
        }
        return "";
    }

    function loadExchageRateList() {
        $http({
            method: 'GET',
            url: 'kursna_lista.json'
        })
            .then(
            function (response) {
                $scope.exchangeRatesTable = response.data.result;
                $scope.exchangeRatesTable['rsd'] = { 'kup': '1', 'sre': '1', 'pro': '1' };
				$scope.onChangeLeft();
            },
            function (response) {
                window.alert('Unable to load the exchange rates list.');
            });
    }

    restoreState();
});