# README #

This README would normally document whatever steps are necessary to get your application up and running.

### How do I get set up? ###

* Make sure that you have NodeJs installed on your computer
* Open command prompt
* Create a directory on your computer, `$dir`
* Switch to the new empty directory, `cd $dir` 
* `git clone https://ivanareal84@bitbucket.org/ivanareal84/currency-calculator.git`
* `cd currency-calculator/CurrencyCalculator`
* `npm install` - downloads nodejs modules
* `node app.js`, starts the nodejs server on port 3000
* Open your internet browser
* Enter URL: `http://localhost:3000`

